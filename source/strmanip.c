#include "strmanip.h"

char** generate_words_list(char* inputs, int* n) {
    int wordcount = 0;
    char* p = inputs;
    int tracking = 0;
    
    char filterstring[] = {32, 0, 10, 46, 44, 40, 41, 91, 93, 63, 64, 58, 59, 47, 34, 9, 130, 132, 145, 146, 147, 148, 123, 125, 126, 150};
    while (*p) {
        if (tracking && anyof(*p, filterstring, sizeof(filterstring))) {            // tracking AND any specific
            ++wordcount;
            tracking = 0;
            *p = '\0';
        } else if (!tracking && !anyof(*p, filterstring, sizeof(filterstring))) {   // not tracking AND none specific
            tracking = 1;
        } else if (!tracking) {                                                     // not tracking AND any specific
            *p = '\0';
        }
        ++p;
    }

    *n = wordcount;
    char** words = mymalloc(wordcount*sizeof(char**));

    // link each address in words with a word in inputs, replace deliminators with '\0's
    tracking = 0;
    p = inputs;
    for (int i = 0; i<wordcount; ++p) {
        if (!tracking && *p != '\0') {
            tracking = 1;
            *(words+i) = p;
            ++i;
        } else if (*p == '\0') {
            tracking = 0;
        }
    }

    return words;
}

int uniq(char** words, int size) {
    int count = 0;
    int current = 0;
    char** currentword = words;
    while (current < (size - 1)) {
        *(words+count) = *(words + current);
        int probeforward = current + 1;
        while (probeforward < size && !mystrcmp(words + count, words + probeforward)) {
            ++probeforward;
        }

        current = probeforward;
        ++count;
    }
    return count;
}
