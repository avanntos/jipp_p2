#include "utility.h"
#include "common.h"

void* mymalloc(int size) {
    void* mem = malloc(size);

    if (!mem) {
        myerr(1, "");
    }

    return mem;
}

int anyof(char c, char* arr, int size) {

    while (size) {
        if (c == *arr) { 
            return 1;
        }
        ++arr;
        --size;
    }

    return 0;
}

char* read_file_inputs(char* filename) {

    // open the file and check the status of the system call
    FILE* fd = fopen(filename, "r");
    if (!fd) {
        myerr(2, filename);
    }

    char buf[BUFFER_SIZE];
    char* read_data = NULL;
    int blocksnr = 0;
    // read contents of the file, appending it to the dynamically allocated array of chars
    // if necessary, reallocate to a bigger chunk of memory
    // use fwrite instead of fgets
    while (fread(buf, sizeof(char), BUFFER_SIZE, fd)) {
        // fputs(buf, stdin);
        // on the first iteration, while read_data==NULL, realloc acts like malloc
        read_data = realloc(read_data, (1 + blocksnr)*BUFFER_SIZE);

        // handle the case when system doesn't give us any memory
        if (!read_data) {
            myerr(1, "Buffering input data.");
        }

        // copy the read data, on iterations > 0th it also takes care of removing '\0' for us
        memcpy(read_data + blocksnr*(BUFFER_SIZE), buf, BUFFER_SIZE);

        // increase number of blocks
        ++blocksnr;
        // puts("block added");
    }
    fclose(fd);

    return read_data;
}

char* getqueryword(int argc, char** argv) {

    if (argc > 1)
        return(*(argv+1));

    puts("Enter a word you want to search for:");

    char* buffer = mymalloc(100*sizeof(char));
    int res = scanf("%s", buffer);

    if (res != 1) {
        myerr(3, "Incorrent scanf return value.");
    }

    return buffer;
}


int mystrcmp(const void* lhs, const void* rhs) {
    char* lhs_c = *((char**)lhs);
    char* rhs_c = *((char**)rhs);
    while (*lhs_c && *rhs_c) {
        int lhs_v = char_to_val(*lhs_c);
        int rhs_v = char_to_val(*rhs_c);
        if (lhs_v > rhs_v)  return 1;
        if (lhs_v < rhs_v)  return -1;
        ++lhs_c;    ++rhs_c;
    }

    if (!*lhs_c && *rhs_c)
        return -1;
    if (*lhs_c && !*rhs_c)
        return 1;

    return 0;
    
}


    int char_to_val(unsigned char c) {
        
        switch (c) {
            // upper case
            case 165:
                return ('A'<<1)+1;
            case 198:
                return ('C'<<1)+1;
            case 202:
                return ('E'<<1)+1;
            case 163:
                return ('L'<<1)+1;
            case 209:
                return ('N'<<1)+1;
            case 211:
                return ('O'<<1)+1;
            case 140:
                return ('S'<<1)+1;
            case 143:
                return ('Z'<<1)+1;
            case 175:
                return ('Z'<<1)+2;
            // lower case
            case 185:
                return ('a'<<1)+1;
            case 230:
                return ('c'<<1)+1;
            case 234:
                return ('e'<<1)+1;  
            case 179:
                return ('l'<<1)+1;
            case 241:
                return ('n'<<1)+1;
            case 243:
                return ('o'<<1)+1;
            case 156:
                return ('s'<<1)+1;
            case 159:
                return ('z'<<1)+1;
            case 191:
                return ('z'<<1)+2;

            default:
                return c<<1;
        }

    }