#include "bsearch.h"


int binary_search(char** words, char* key, int size) {

    int lb = 0, rb = size - 1;
    while (lb <= rb) {
        int mid = (rb + lb)/2;
        int cmp = mystrcmp(&key, words+mid);
        switch (cmp) {
            case -1:
                rb = mid - 1;   break;
            case 0:
                return mid;     break; 
            case 1:
                lb = mid + 1;   break;
        }

    }
    return -1;
}