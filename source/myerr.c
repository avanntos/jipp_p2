#include "myerr.h"

static char* errtexts[] = { "",
                            "Error allocating memory",
                            "Error opening file",
                            "Error reading user input",
                            "Unknown error"};

static const int MAXERR = sizeof(errtexts)/sizeof(char*) - 1;

void myerr(int errnr, char* error_string) {

    int errid = errnr > MAXERR ? MAXERR : errnr;
    fprintf(stderr, "Critical error encountered while running.\n"
                    "error code: %d -> error desc: %s.\n"
                    "Additional error information (if any): [%s].\n",
                    errnr, errtexts[errid], error_string);

    exit(errnr);
}