#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

#include "utility.h"
#include "bsearch.h"
#include "strmanip.h"
#include "myerr.h"


int main(int argc, char** argv) {

    if(!setlocale(LC_ALL, "pl_PL.cp1250")) {
        myerr(10, "not able to set locale correctly (pl_PL.cp1250)");
    }

    char* inputs = read_file_inputs("input2.txt");

    int wordcount = 0;
    char** words = generate_words_list(inputs, &wordcount);

    qsort(words, wordcount, sizeof(char**), mystrcmp);

    wordcount = uniq(words, wordcount);

    char* keyword = getqueryword(argc, argv);
    int wordid = binary_search(words, keyword, wordcount);

    if (wordid != -1) {
        printf("[%s] found at index=%d\n", keyword, wordid);
    } else {
        printf("[%s] not found within the input data\n", keyword);
    }

    free(words);    free(inputs);
    if (argc == 1)  free(keyword);

    return 0;
}