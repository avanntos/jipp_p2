#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "myerr.h"

void* mymalloc(int) ;

int anyof(char, char*, int) ;

char* read_file_inputs(char*) ;

char* getqueryword(int, char**) ;

int mystrcmp(const void*, const void*) ;

int char_to_val(unsigned char) ;
